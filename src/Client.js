import axios from "axios"

export class Client{
    constructor(gameId) {
        this.gameId = gameId;
        this.baseUrl = "https://www.tabutime.com/api";
        //this.baseUrl = "http://0.0.0.0:2000/api";
        this.gameUrl = this.baseUrl + "/games";
        this.tabooUrl = this.baseUrl + "/taboos";
    }
    async getGameDetails(){
        const res = await axios.get(this.gameUrl + "/" + this.gameId);
        return res.data;
    }
    async getTurn(){
        const res = await axios.get(this.gameUrl + "/turn/" + this.gameId);
        return res.data;
    }
    async getNewWord(){
        const res = await axios.get(this.gameUrl + "/taboo/" + this.gameId);
        return res.data.data;
    }
    async getWordById(taboo){
        const res = await axios.get(this.tabooUrl + "/" + taboo);
        return res.data.data;
    }
    sendScore(team, score){
        const data = {
            "team": team,
            "score": score
        }
        axios.put(this.gameUrl + "/" + this.gameId, data)
            .catch(err => console.log(err));
    }
    configureGame(turnTime, targetScore, teamCount, teams){
        const data = {
            "turnTime": turnTime,
            "targetScore": targetScore,
            "teamCount": teamCount,
            "teams": teams
        }
        axios.put(this.gameUrl + "/configure/" + this.gameId, data)
            .catch(err => console.log(err));
    }
    passTurn(){
        axios.put(this.gameUrl + "/turn/" + this.gameId, {"dummy": "data"})
            .catch(err => console.log(err));
    }
    async addPlayer(playerName){
        return await axios.post(this.gameUrl + "/" + this.gameId, {
            "name": playerName
        }).catch((err) => {
            console.log(err);
            alert("Oyuncu oluşturulamadı\nAynı isimde biri olabilir mi?");
            window.location.reload();
        });
    }
    updateTaboo(taboo){
        axios.put(this.gameUrl + "/taboo/" + this.gameId, {"taboo": taboo})
            .catch(err => console.log(err));
    }
    updateTimeLeft(timeLeft){
        axios.put(this.gameUrl + "/time/" + this.gameId, {"timeLeft": timeLeft})
            .catch(err => console.log(err));
    }
}