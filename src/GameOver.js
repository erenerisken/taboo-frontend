import React from "react";
import "./GameOver.css"
export class WinnerCard extends React.Component {
    constructor(props) {
        super(props);

        this.link = "https://gitlab.com/erenerisken/taboo-frontend/-/raw/master/images/winner.gif";
    }
    render() {
        return (
            <div className={"gameover-card"}>
                <img src={this.link}
                     alt={"winner"}/>
                <button className={"gameover-reload"} onClick={() => window.location.reload()}>
                    Yeniden Oyna!
                </button>
            </div>
        )
    }
}

export class LoserCard extends React.Component {
    constructor(props) {
        super(props);

        this.link = "https://gitlab.com/erenerisken/taboo-frontend/-/raw/master/images/loser.gif";
    }
    render() {
        return (
            <div className={"gameover-card"}>
                <img src={this.link}
                     alt={"loser"}/>
                <button className={"gameover-reload"} onClick={() => window.location.reload()}>
                    Yeniden Oyna!
                </button>
            </div>
        )
    }
}