import React from "react";
import "./PreGame.css"

export class PreGame extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            gameId: "",
            playerName: ""
        }
        document.title = "Tabu Giriş";
        this.handlePlayerName = this.handlePlayerName.bind(this);
        this.handleGameId = this.handleGameId.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(){
        if(this.state.gameId.length < 4){
            alert("Oyun Adı en az 4 karakter olmalıdır.");
            return;
        }
        if(this.state.playerName.length < 4){
            alert("İsminiz en az 4 karakter olmalıdır.");
            return;
        }
        this.props.onReady(this.state);
    }

    handleGameId(event){
        this.setState({gameId: event.target.value});
    }

    handlePlayerName(event){
        this.setState({playerName: event.target.value});
    }

    render() {
        return (
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-25"}>
                        <label>Oyun Adı</label>
                    </div>
                    <div className={"col-75"}>
                        <input type={"text"} id={"gameId"} name={"gameId"} maxLength={14} minLength={4}
                        placeholder={""} onChange={this.handleGameId}/>
                    </div>
                </div>
                <div className={"row"}>
                    <div className={"col-25"}>
                        <label>İsminiz</label>
                    </div>
                    <div className={"col-75"}>
                        <input type={"text"} id={"playerName"} name={"playerName"} maxLength={14} minLength={4}
                               placeholder={""} onChange={this.handlePlayerName}/>
                    </div>
                </div>
                <div className={"row"}>
                    <div className={"center-button"}>
                        <input type={"submit"} value={"Onayla"} onClick={this.handleSubmit}/>
                    </div>
                </div>
            </div>
        )
    }
}