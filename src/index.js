import React from 'react';
import ReactDOM from 'react-dom';
import {Board} from "./Board";
import {PreGame} from "./PreGame";
import './index.css';

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.gameId = "";
        this.player = "";
        this.state = {
            ready: false
        }
    }
    handleSubmit(state){
        this.gameId = state.gameId;
        this.player = state.playerName;
        this.setState({ready: true});
    }
    renderGame() {
        if(this.state.ready)
          return (
            <div className="game">
              <div className="game-board">
                <Board gameId={this.gameId} playerName={this.player}/>
              </div>
            </div>
          );
        else
            return (
                <div className={"game"}>
                    <div className={"game-board"}>
                        <PreGame onReady={(state) => this.handleSubmit(state)}/>
                    </div>
                </div>
            )
    }

    render() {
        return (
            <div>
                <div className={"banner"}>
                    <img src={"https://gitlab.com/erenerisken/taboo-frontend/-/raw/master/public/logo192.png"} height={"100pt"}/>
                    <div className={"banner-typo"}>
                        Tabu Time!
                    </div>
                </div>
                {this.renderGame()}
            </div>
        );
    }
}

// ========================================

ReactDOM.render(

    <Game />,
  document.getElementById('root')
);

