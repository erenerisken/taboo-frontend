import React from "react";
import {randomInt} from "./utils";
import './GameConfiguration.css'

export class GameConfiguration extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            teamCount: 2,
            turnTime: 60,
            targetScore: 30,
            isScoreLimited: false,
            choices: Array(0)
        }
        const defaultChoices = Array(0);
        for(let i = 0; i<this.props.allPlayers.length; i++){
            defaultChoices.push(0);
        }
        this.setState({choices: defaultChoices});
    }
    onChoiceChange(i, value){
        const newChoice = this.state.choices;
        newChoice[i] = Number(value);
        this.setState({choices: newChoice});
    }
    handleScoreLimitCheck(event){
        this.setState({isScoreLimited: event.target.checked});
    }
    handleScoreLimit(event){
        this.setState({targetScore: event.target.value});
    }
    randomDistribute(){
        const n = this.props.allPlayers.length;
        const permutation = Array(0);
        const newChoices = this.state.choices.slice();
        for (let i = 0; i<n; i++){
            permutation.push(i);
        }
        for (let i = 0; i<n; i++){
            const j = randomInt(0, n-1);
            [permutation[i], permutation[j]] = [permutation[j], permutation[i]];
        }
        for (let i = 0; i<this.state.teamCount; i++){
            const numberOfPlayersInTeam = (i+1) <= n % this.state.teamCount ?
                Math.floor(n / this.state.teamCount + 1):
                Math.floor(n / this.state.teamCount);
            console.log("Player count in team " + (i+1) + " = " + numberOfPlayersInTeam);
            for (let j = 0; j<numberOfPlayersInTeam; j++){
                const player = permutation.pop();
                newChoices[player] = i;
            }
        }
        this.setState({choices: newChoices});
    }
    renderOption(i){
        return (
            <option value={i}>{"Takım " + (i+1)}</option>
        )
    }
    renderChoice(i){
        const options = Array(0);
        for(let j = 0; j<this.state.teamCount; j++){
            options.push(this.renderOption(j));
        }
        options.push(<option value={-1}>Oyundan At</option> );
        return (
            <div className={"row"}>
                <div className={"conf-col-50"}>
                    <label>{this.props.allPlayers[i].name}</label>
                </div>
                <div className={"conf-col-50"}>
                    <select className={"conf-dropdown"}
                            onChange={(event) => this.onChoiceChange(i, event.target.value)}
                            value={this.state.choices[i]}>
                        {options}
                    </select>
                </div>
            </div>
        )
    }
    onTeamCountChange(diff){
        const newTeamCount = Math.max(this.state.teamCount + diff, 2);
        if(this.state.teamCount !== newTeamCount){
            this.setState({teamCount: newTeamCount});
        }
    }
    onTurnTimeChange(event){
        this.setState({turnTime: event.target.value});
    }
    onConfig(){
        const teams = Array(0);
        const targetScore = this.state.isScoreLimited ? this.state.targetScore : -1;
        for(let i = 0; i<this.state.teamCount; i++){
            teams.push(Array(0));
        }
        for(let i = 0; i<this.state.choices.length; i++){
            if(this.state.choices[i] === -1){
                continue;
            }
            teams[this.state.choices[i]].push(this.props.allPlayers[i].name);
        }
        this.props.onConfig(this.state.turnTime, targetScore, teams);
    }
    render() {
        if(this.props.allPlayers.length > this.state.choices.length){
            const newChoices = this.state.choices.slice();
            for(let i = 0; i<this.props.allPlayers.length - this.state.choices.length; i++){
                newChoices.push(0);
            }
            this.setState({choices: newChoices});
        }
        return (
            <div className={"conf-container"}>
                <div className={"row"}>
                    <div className={"conf-col-50"}>
                        <label>Takım sayısı</label>
                    </div>
                    <div className={"conf-col-25"}>
                        <label>{this.state.teamCount}</label>
                    </div>
                    <div className={"conf-col-25"}>
                        <div className={"row"}>
                            <button className={"conf-team"} onClick={() => this.onTeamCountChange(-1)}>-</button>
                            <button className={"conf-team"} onClick={() => this.onTeamCountChange(1)}>+</button>
                        </div>
                    </div>
                </div>
                <div className={"conf-center-button"}>
                    <button className={"conf-button"} onClick={() => this.randomDistribute()}>Rastgele Takım</button>
                </div>
                {this.props.allPlayers.map((value, index) => this.renderChoice(index))}
                <div className={"row"}>
                    <div className={"conf-col-50"}>
                        <label>Tur süresi</label>
                    </div>
                    <div className={"conf-col-50"}>
                        <select onChange={(event) => this.onTurnTimeChange(event)}
                                value={this.state.turnTime}>
                            <option value={60}>60 sn</option>
                            <option value={90}>90 sn</option>
                            <option value={120}>120 sn</option>
                        </select>
                    </div>
                </div>
                <div className={"row"}>
                    <div className={"conf-col-50"}>
                        <label>Skor limiti</label>
                    </div>
                    <div className={"conf-col-50"}>
                        <input name={"isScoreLimited"} type={"checkbox"} className={"score-limit-box"}
                               checked={this.state.isScoreLimited}
                               onChange={event => this.handleScoreLimitCheck(event)}/>
                    </div>
                </div>
                {this.state.isScoreLimited ?
                    <div className={"row"}>
                        <div className={"conf-col-50"}>
                            <label>Hedef skor</label>
                        </div>
                        <div className={"conf-col-50"}>
                            <input className={"score-limit-form"} name={"scoreLimit"} type={"number"} min={10}
                                   value={this.state.targetScore}
                                   onChange={event => this.handleScoreLimit(event)}/>
                        </div>
                    </div>: null}
                <div className={"conf-center-button"}>
                    <button className={"conf-button"} onClick={() => this.onConfig()}>Onayla</button>
                </div>
            </div>
        )
    }
}