import React from "react";

export class ReadyPrompt extends React.Component{
    render() {
        return (
            <div className={"ready-prompt"}>
                <div className={"ready-label"}>Anlatma sırası sende!</div>
                <button className={"ready-button"} onClick={this.props.onClick}>Hazır</button>
            </div>
        )
    }
}