import React from "react";
import Jitsi from "react-jitsi";
import {isMobile} from 'react-device-detect'

const interfaceConfig = {
    LANG_DETECTION: false,
    lang: "es",
    APP_NAME: "QoriMed",
    DISABLE_JOIN_LEAVE_NOTIFICATIONS: true,
    HIDE_INVITE_MORE_HEADER: true,
    MOBILE_APP_PROMO: false,
    SHOW_CHROME_EXTENSION_BANNER: false,
    TOOLBAR_BUTTONS: [
        "microphone",
        "camera",
        "fullscreen",
        "fodeviceselection",
        "hangup",
        "profile",
        "settings",
        "videoquality",
        "tileview",
        "download",
        "help",
        "mute-everyone"
        // 'security'
    ]
};

const config = {
    defaultLanguage: "es",
    prejoinPageEnabled: false,
    disableDeepLinking: true
};
export class VideoChat extends React.Component{
    constructor(props) {
        super(props);
        this.handleAPI = JitsiMeetAPI => {
            JitsiMeetAPI.executeCommand("toggleVideo");
        };
        this.state = {
            join: false,
            display: true
        }
    }
    /*
    componentDidMount() {
        navigator.mediaDevices.getUserMedia({audio: true, video: true},
            () => alert("success"), () => alert("fail"));
    }*/

    render(){
        return this.state.display?(
            <div className={"video-chat"}>
                {this.state.join?
                    <div>
                        <Jitsi
                            containerStyle={isMobile ? {width: '360px', height: '202px'} :null}
                            domain="meet.jit.si"
                            onAPILoad={this.handleAPI}
                            roomName={this.props.roomName}
                            displayName={this.props.username}
                            interfaceConfig={interfaceConfig}
                            config={config}
                        />
                        <button onClick={() => this.setState({display: false})}
                                className={"video-close"}>Kapat</button>
                    </div>:
                    <button onClick={() => this.setState({join: true})}
                            className={"video-join"}>Görüntülü Konuşma</button>
                }
            </div>
        ): null;
    }
}