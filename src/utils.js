export default function timeToString(t) {
    const min = Math.floor(t/60);
    const sec = t % 60;
    return min + ":" + (sec > 9 ? "" : "0") + sec;
}

export function randomInt(a, b){
    return Math.floor(Math.random() * (b - a + 1)) + a;
}