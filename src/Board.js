import React from "react";
import {BrowserView, MobileView} from 'react-device-detect'
import timeToString from "./utils";
import {Client} from "./Client"
import {ReadyPrompt} from "./ReadyPrompt";
import {VideoChat} from "./Chat";
import {GameConfiguration} from "./GameConfiguration";
import {WinnerCard, LoserCard} from "./GameOver";

function Square(props) {
    return <button className={props.type} onClick={props.onClick}>
        {props.value}
    </button>
}


export class Board extends React.Component {
    constructor(props) {
        super(props);
        document.title = "Tabu Time!";
        this.gameId = props.gameId;
        this.playerName = props.playerName;
        this.client = new Client(this.gameId);
        this.restarted = false;
        this.readyForNextTurn = false;
        this.lastTurn = "";
        this.playerCreated = false;
        this.configured = false;
        this.tabooId = 0;
        this.currentTeam = -1;
        this.turnTime = 0;
        this.admin = "";
        alert("Oyun başlıyor.");
        this.state = {
            targetWord: "Kelime",
            bannedWords: ["Yasak1", "Yasak2", "Yasak3", "Yasak4", "Yasak5"],
            remainingPass: 2,
            team: -1,
            scores: [],
            timeRemaining: this.turnTime,
            winner: -1,
            playing: false,
            interval: setInterval(() => this.handleInterval(), 1000),
            askNextTurn: false,
            configure: false,
            allPlayers: []
        };
    }

    /**
     * Update score table and taboo.
     * Check if turn is player's.
     *
     * @return {boolean}
     */
    turnRestarted(){
        this.client.getGameDetails()
        .then((data) => {
            if(!this.configured && data['configured']){
                this.turnTime = data['turnTime'];
                const teams = data['players'];
                for(let i = 0; i<teams.length; i++){
                    let found = false;
                    //eslint-disable-next-line
                    teams[i].map(player => {
                        if(player['name'] === this.playerName){
                            found = true;
                            this.setState({team: i});
                        }
                    });
                    if(found){
                        break;
                    }
                }
            }
            this.configured = data['configured'];
            this.admin = data['admin'];
            if(!this.configured && this.playerName === data['admin']){
                this.setState({configure: true, allPlayers: data['allPlayers']});
            }
            else if(this.configured && this.state.configure){
                this.setState({configure: false});
            }
            this.setState({scores: data['scores']});
            if(!this.playerCreated){
                this.playerCreated = true;
                this.client.addPlayer(this.playerName).then(() => this.playerCreated = true)
                    .catch(err => console.log(err));
            }
            if(!this.state.playing){
                this.setState({timeRemaining: data["timeLeft"]});
                this.client.getWordById(data["taboo"]).then(data => {
                    this.updateWord(data)
                });
            }
        });
        if(this.restarted && this.playerCreated && this.configured){
            if(!this.state.askNextTurn){
                this.setState({askNextTurn: true});
            }
            if(this.readyForNextTurn) {
                this.restarted = false;
                this.readyForNextTurn = false;
                this.setState({askNextTurn: false});
                return true;
            }
            return false;
        }
        if(this.playerCreated && this.configured){
            this.client.getTurn().then((data) => {
                if(data["winner"] >= 0){
                    this.setState({winner: data["winner"]});
                }
                else{
                    this.restarted = data["player"] === this.playerName &&
                        this.state.team === data['team'] && this.lastTurn !== this.playerName;
                    this.lastTurn = data["player"];
                    this.currentTeam = data["team"];
                }
            })
        }
        return false;
    };
    initNewTurn(){
        this.nextWord();
        this.setState({timeRemaining: this.turnTime, remainingPass: 2, playing: true});
    };
    handleInterval(){
        const newRemaining = this.state.timeRemaining - 1;
        if(this.state.timeRemaining === 0){
            if(this.state.playing){
                this.passTurn();
            }
            this.setState({playing: false});
        }
        if(!this.state.playing){
            if(this.turnRestarted()){
                this.initNewTurn();
            }
        }
        else{
            this.setState({timeRemaining: newRemaining});
            this.client.updateTimeLeft(newRemaining);
        }
    };
    updateWord(data){
        this.tabooId = data["id"];
        this.setState({targetWord: data["word"], bannedWords: data["taboos"]});
    }
    nextWord(){
        this.client.getNewWord().then(data => {
            this.updateWord(data);
            this.client.updateTaboo(this.tabooId);
        })
    };
    passTurn(){
        this.client.passTurn();
    }
    sendScore(){
        this.client.sendScore(this.state.team, this.state.scores[this.state.team]);
    };
    handlePass(){
        if(this.state.remainingPass <= 0)
            return;
        this.nextWord();
        this.setState({remainingPass: this.state.remainingPass-1});
    };
    handleCorrect(){
        const newScores = this.state.scores;
        newScores[this.state.team]++;
        this.setState({scores: newScores});
        this.nextWord();
        this.sendScore();
    };
    handleFalse(){
        const newScores = this.state.scores;
        newScores[this.state.team]--;
        this.setState({scores: newScores});
        this.nextWord();
        this.sendScore();
    };
    handleConfigure(turnTime, targetScore, teams){
        const teamCount = teams.length;
        this.client.configureGame(turnTime, targetScore, teamCount, teams);
    };
    renderSquare(i) {
        return <div>
            <Square
                value={this.state.bannedWords[i]}
                type={"banned"}
                onClick={null}
            />
        </div>
    }
    renderButtons(){
        if(!this.state.playing)
            return null;
        return  <div className={"board-row"}>
            <Square value={"Pas (" + this.state.remainingPass + ")"} type={"pass"} onClick={() => this.handlePass()} />
            <Square value={"Doğru"} type={"correct"} onClick={() => this.handleCorrect()} />
            <Square value={"Tabu"} type={"false"} onClick={() => this.handleFalse()} />
        </div>
    }
    renderScore(i){
        return (
            <div className={"status"}>{"Takım " + (i+1) + ": " + this.state.scores[i]}</div>
        )
    }
    render() {
        const gameLabel = this.gameId === "" ? "" : ("Oyun Kodu: " + this.gameId);
        const currentTurn = this.lastTurn !== "" ?
            "Anlatan: " + this.lastTurn + "  (takım " + (this.currentTeam + 1) + ")":
            "Oyun henüz başlamadı.";
        let timeRem = timeToString(this.state.timeRemaining);
        const boardRow = (this.currentTeam !== this.state.team || this.lastTurn === this.playerName) ?
            <div className="board-row">
                <Square value={this.state.targetWord} type={"target"} onClick={null}/>
                {this.state.bannedWords.map((value, index) => this.renderSquare(index))}
            </div> : null;
        return (
            <div className={"game-row"}>
                <div>
                    { this.state.winner === -1 ? (
                    <div>
                        <div className={"game-info"}>
                            <div className={"status"}>
                                {this.configured?currentTurn:"Moderatör " + this.admin + " bekleniyor"}
                            </div>
                            <div className="status">{"Kalan süre : " + timeRem}</div>
                            {this.state.scores.map((value, index) => this.renderScore(index))}
                            <div className="status">{gameLabel}</div>
                        </div>
                        {boardRow}
                        {this.renderButtons()}
                        {this.state.askNextTurn ? <ReadyPrompt onClick={() => this.readyForNextTurn = true} /> : null}
                    </div>
                        ) : (
                    <div>
                        {this.state.winner === this.state.team ?
                            <WinnerCard /> :
                            <LoserCard />}
                    </div>
                    )}
                    {this.state.configure ?
                        <GameConfiguration allPlayers={this.state.allPlayers}
                                           onConfig={(turnTime, targetScore, teams) =>
                                               this.handleConfigure(turnTime, targetScore, teams)}/> :
                        null
                    }
                    <MobileView>
                        <VideoChat username={this.playerName} roomName={"tabutime-" + this.gameId} />
                    </MobileView>
                </div>
                <BrowserView>
                    <VideoChat username={this.playerName} roomName={"tabutime-" + this.gameId} />
                </BrowserView>
            </div>
        );
    }
}